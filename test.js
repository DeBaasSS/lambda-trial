var box = require('box-view'),
    async = require("async"),
    zlib = require('zlib'),
    fs = require('fs'),
    unzip = require('unzip'),
    LanguageDetect = require('languagedetect'),
    AWS = require("aws-sdk"),
    config = require('./config/settings.json');

var boxViewApiToken = 'z9z3xv0ob4v4440q6s4mavl13lnnrmhe';
var documentId = '017358c20c27460f8a3f112c767ff7b1';

// Set up Box client
var client = box.createClient(boxViewApiToken);

// Set up AWS client
AWS.config.loadFromPath('./config/aws.json');

// Set up S3 client
var s3 = new AWS.S3();

// Setup Language detect client
var lngDetector = new LanguageDetect();

// function getPageCount() {
//   fs.readFile('./output/assets/info.json', function(output){
//     console.log(output);
//   })
// }

console.log('They see me rollin');

async.waterfall([

   // Fetch the zip file from Box and write to disk
   function(callback) {
     console.log('Fetching zip file from Box...');
     client.documents.getContent(documentId, { extension: 'zip' }, function (err, res) {
         if (err) {
             console.error(err);
             return;
         }

         // `res` is the HTTP Response object
         res.pipe(fs.createWriteStream('./doc.zip').on('finish', function(){
           callback(null);
         }));
     });
   },

   // Unzip
   function(callback) {
     console.log('Unzipping...');

      var r = fs.createReadStream('./doc.zip');
      var u = unzip.Extract({ path: './output' });

      r.pipe(u);
      r.on('end', function() {
        console.log('Assets unzipped');
        console.log(fs.readdirSync('./output/assets'));
        callback(null);
      });
   },

   // Validate required files
   function(callback) {
     var files = fs.readdirSync('./output/assets');
     var missing = false;

     console.log('Validating required files...');

     config.requiredAssets.forEach(function(file){
       console.log('Checking ' + file);

         if (fs.existsSync("./output/assets/" + file)) {
           console.log(file + ' exists.');
         } else {
           console.log('Missing ' + file);
           missing = true;
         }
     });

     if ( ! missing) {
       callback(null, files);
     } else {
       callback('Critical assets missing.');
     }
   },



   // Copy assets to S3 bucket
   function(files, callback) {
     console.log('Uploading to S3...');

     files.forEach(function(file) {
       console.log('Uploading ' + file);
       var stream = fs.createReadStream('./output/assets/' + file).pipe(zlib.createGzip());

       var params = {Bucket: 'studeersnel.lambda.test', Key: documentId + '/' + file, Body: stream};
       var options = {partSize: 10 * 1024 * 1024, queueSize: 1};
        s3.upload(params, options, function(err, data) {
          console.log(err, data);
        });
     });

     callback(null);
   },

   function(callback) {

     console.log('Fetching txt file from Box...');
     client.documents.getContent(documentId, { extension: 'txt' }, function (err, res) {
         if (err) {
             console.error(err);
             return;
         }

         // `res` is the HTTP Response object
         res.pipe(fs.createWriteStream('./output/' + documentId + '.txt').on('finish', function(){
          fs.readFile('./output/' + documentId + '.txt', {"encoding": "utf-8"}, function (err, data) {
            if (err) throw err;

            var result = lngDetector.detect(data);
            var language = result[0][0];
            var likelyhood = result[0][1];

            if(likelyhood > config.minimumLikelyhood) {
              console.log('Detected ' + language + ' with ' + likelyhood + ' likelyhood');
            } else {
              console.log('Could not detect language');
            }
          });

           callback(null);
         }));

     });
   }


 // optional callback for results
 ], function (err, result) {
   //if (err) context.done(err, "Drat!!");
   //if (!err) context.done(null, "Code successfully pushed to github.");
   if (err) console.log(err);
   if (!err) console.log("All done here!");

   //fs.unlink('./output/assets');
 });
