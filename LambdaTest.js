var box = require('box-view'),
  async = require("async"),
  AWS = require("aws-sdk");

var boxViewApiToken = 'z9z3xv0ob4v4440q6s4mavl13lnnrmhe';
var documentId = '017358c20c27460f8a3f112c767ff7b1';

exports.handler = function(event, context) {
    console.log('They see me rollin');

    // Set up AWS client
    AWS.config.loadFromPath('./aws/config.json');
    var s3 = new AWS.S3();

    // Set up Box client
    //var myKey = process.env.BOX_VIEW_API_TOKEN;
    var client = box.createClient(boxViewApiToken);

    client.documents.get(documentId, function (err, doc, res) {
        // `doc` is the JSON-parsed response body
        // `res` is the HTTP Response object
        console.log('Document response:');
        console.log(doc);

        context.done(null, 'All done here');  // SUCCESS with message
    });

    var params = {Bucket: 'studeersnel.lambda.test', Key: 'test.zip'};
    //var file = require('fs').createWriteStream('/path/to/file.jpg');


    client.documents.getContent(documentId, { extension: 'zip' }, function (err, res) {
        if (err) {
            console.error(err);
            return;
        }

        // `res` is the HTTP Response object
        //res.pipe(fs.createWriteStream('./doc.zip'));
        s3.getObject(params).createReadStream().pipe(res);
    });

};
